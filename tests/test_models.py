import pytest as pytest
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

from app.db import Base
from app.models import User

engine = create_engine('sqlite:///:memory:', echo=True)
Session = scoped_session(sessionmaker(bind=engine))
inspector = sqlalchemy.inspect(engine)


@pytest.fixture(scope="module")
def db_session() -> Session:
    Base.metadata.create_all(engine)
    session = Session()
    yield session
    session.close()
    Base.metadata.drop_all(bind=engine)


@pytest.fixture
def user(db_session):
    user = User(username='user', email='user@email.com', first_name='First', last_name='Last')
    db_session.add(user)
    db_session.commit()
    yield user


class TestUser:
    def test_create(self, db_session) -> None:
        user = User(username='artyom', email='email@email.com')
        user.first_name = 'Artyom'
        user.last_name = 'Sviridov'
        db_session.add(user)
        db_session.commit()
        assert user is db_session.query(User).filter_by(id=user.id).scalar()

    def test_create_name_drop_table(self, user, db_session):
        user_dt_comments = User(username="danger_user; DROP TABLE users--", email="danger_user@email.com")
        user_dt = User(username="danger; DROP TABLE users;", email="danger@email.com")
        db_session.add_all((user_dt_comments, user_dt))
        db_session.commit()
        assert True is inspector.has_table("users")
